﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace JSerializer.Models
{
    public class FileItem
    {
        public String Name { get; set; }
        public String Size { get; set; }
        public String Path { get; set; }

        public FileItem(String path)
        {
            this.Name = System.IO.Path.GetFileName(path);
            this.Size = new FileInfo(path).Length.ToString() + " " + "B";
            this.Path = path;
        }

    }
}
