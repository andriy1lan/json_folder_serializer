﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace JSerializer.Models
{
    public class Folder
    {

        public String Name { get; set; }
        public String DateCreated { get; set; }
        public List<FileItem> Files { get; set; }
        public List<Folder> Children { get; set; }

        public Folder(String path)
        {

            this.Files = new List<FileItem>();
            this.Children = new List<Folder>();
            this.Name = new DirectoryInfo(path).Name;
            this.DateCreated = Directory.GetCreationTime(path).ToString("dd-MMM-yy h:mm tt");

            //Fill the list of files description, if exists
            foreach (String f in Directory.GetFiles(path))
            {
                this.Files.Add(new FileItem(f));
            }

            //Fill the list of subfolders, if exists
            foreach (String d in Directory.GetDirectories(path))
            {
                this.Children.Add(new Folder(d));
            }

        }
    }
}

