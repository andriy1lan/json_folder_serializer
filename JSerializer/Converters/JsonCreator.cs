﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JSerializer.Models;

namespace JSerializer.Converters
{
    public class JsonCreator
    {
        public static String Indent(int count)
        {
            return "".PadLeft(count);
        }

        public static String JsonFile(FileItem file, String pad)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(pad + Indent(12) + "{");
            sb.AppendLine(pad + Indent(16) + "\"Name\"" + ": " + '"' + file.Name + '"' + ",");
            sb.AppendLine(pad + Indent(16) + "\"Size\"" + ": " + '"' + file.Size + '"' + ",");
            sb.AppendLine(pad + Indent(16) + "\"Path\"" + ": " + '"' + file.Path + '"');
            sb.Append(pad + Indent(12) + "}"); //+","+"\r\n"
            return sb.ToString();
        }


        public String JsonFolder(Folder fol, int k)
        {
            k++;
            int l = k;
            String spaces = Indent(20 * l);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(spaces + "{");
            sb.AppendLine(spaces + Indent(7) + "\"Name\"" + ": " + '"' + fol.Name + '"' + ",");
            sb.AppendLine(spaces + Indent(7) + "\"DateCreated\"" + ": " + '"' + fol.DateCreated + '"' + ",");
            if (fol.Files.Count == 0) sb.AppendLine(spaces + Indent(7) + "\"Files\"" + ": []" + '"' + ",");
            else if (fol.Files.Count > 0)
            {
                sb.AppendLine(spaces + Indent(7) + "\"Files\"" + ": [");
                for (int i = 0; i < fol.Files.Count - 1; i++)
                {
                    sb.AppendLine(JsonFile(fol.Files[i], spaces) + ",");
                }
                sb.AppendLine(JsonFile(fol.Files[fol.Files.Count - 1], spaces));
                sb.AppendLine(spaces + Indent(7) + "],");
            }
            if (fol.Children.Count == 0) sb.AppendLine(spaces + Indent(7) + "\"Children\"" + ": []" + '"' + ",");
            else if (fol.Children.Count > 0)
            {
                sb.AppendLine(spaces + Indent(7) + "\"Children\"" + ": [");
                for (int i = 0; i < fol.Children.Count - 1; i++)
                {
                    sb.AppendLine(JsonFolder(fol.Children[i], k) + ",");
                }
                sb.AppendLine(JsonFolder(fol.Children[fol.Children.Count - 1], k));
                sb.AppendLine(spaces + Indent(7) + "]");

            }
            sb.Append(spaces + "}");
            return sb.ToString();

        }
    }
}
