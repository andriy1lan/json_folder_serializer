﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Forms;
using System.ComponentModel;
using Newtonsoft.Json;
using System.Threading;
using System.Globalization;
using System.IO;
using JSerializer.Converters;
using JSerializer.Models;

namespace JSerializer.ViewModels
{
    public class JSONViewModel : INotifyPropertyChanged 
    {

        private String folname;
        private String filename;
        private String label3;
        private String label5;
        private String label6;

        public String Label3
        {
            get
            {
                return label3;
            }
            set
            {
                label3 = value;
                OnPropertyChanged("Label3");
            }
        }

        public String Label5
        {
            get
            {
                return label5;
            }
            set
            {
                label5 = value;
                OnPropertyChanged("Label5");
            }
        }

        public String Label6
        {
            get
            {
                return label6;
            }
            set
            {
                label6 = value;
                OnPropertyChanged("Label6");
            }
        }

        public ICommand FolderCommand { get; set; }
        public ICommand FileCommand { get; set; }

        public JSONViewModel()
        {
            FolderCommand = new RelayCommand(Executemethod,Canexecutemethod);
            FileCommand = new RelayCommand(Executemethod2, Canexecutemethod);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        private bool Canexecutemethod(object parameter)
        {
            return true;
        }

        private void Executemethod(object parameter)
        {
            var dialog = new FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                folname = dialog.SelectedPath;
                //lb3.Content = dialog.SelectedPath;
                Label3 = dialog.SelectedPath;
            }
            Console.WriteLine(Label3);
        }

        private void Executemethod2(object parameter)
        {
            Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();

            try
            {
                Nullable<bool> result = saveFileDialog.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    filename = saveFileDialog.FileName;
                }
            }

            catch (ArgumentException ex)
            {
                Label5 = "Enter new file name in field " + ex.Message;
            }

            StringBuilder text = new StringBuilder("Input : " + '"' + folname + '"' + "\r\n" + "Output : " + "\r\n");
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Folder fol = new Folder(folname);
            //String json = JsonConvert.SerializeObject(fol, Formatting.Indented);
            String json = CreateCustomJson(fol);
            text.Append(json);
            try
            {
                File.WriteAllText(filename, text.ToString());
                //Alternative: Serialize to JSON with custom jsonconveter saved to constant file
                File.WriteAllText(@"c:\001.txt", new JsonCreator().JsonFolder(fol, -1));
            }
            catch (Exception ex)
            {
                Label5 = "Enter or Select file correctly " + ex.Message;
            }

            Label5 = filename;
            Label6 = "Processed: " + GetAllFoldersCount(folname) + " folders";

        }

        public String CreateCustomJson(Folder folder)
        {
            string json2;
            using (var sw = new StringWriter())
            {
                using (var jtw = new JsonTextWriter(sw)
                {
                    Formatting = Formatting.Indented,
                    Indentation = 5,
                    IndentChar = ' '
                })
                {
                    (new JsonSerializer()).Serialize(jtw, folder);
                }
                json2 = sw.ToString();
            }
            return json2;
        }

        public int GetAllFoldersCount(String folder)
        {
            return Directory.GetDirectories(folder, "*", SearchOption.AllDirectories).Length;
        }

    }
}
