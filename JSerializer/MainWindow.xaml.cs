﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Threading;
using System.Globalization;
using System.IO;
using JSerializer.Models;
using JSerializer.Converters;
using JSerializer.ViewModels;

namespace JSerializer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        //String folname = "";  //Folder for the serialization
        //String filename = ""; //File with JSON structure

        public MainWindow()
        {
            InitializeComponent();
            DataContext = new JSONViewModel();
        }

        /*
        private void Button_Click1(object sender, RoutedEventArgs e) {

           var dialog = new FolderBrowserDialog();
           System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK) {
                folname=dialog.SelectedPath;
                lb3.Content = dialog.SelectedPath;
            }

        }
         

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            
            try
            {
                Nullable<bool> result = saveFileDialog.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    filename = saveFileDialog.FileName;
                }
            }

            catch (ArgumentException ex)
            {
                lb5.Content = "Enter new file name in field " + ex.Message;
            }

            StringBuilder text = new StringBuilder("Input : " + '"' + folname + '"' + "\r\n" + "Output : " + "\r\n");
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Folder fol = new Folder(folname);
            //String json = JsonConvert.SerializeObject(fol, Formatting.Indented);
            String json = CreateCustomJson(fol);
            text.Append(json);
            try
            {
                File.WriteAllText(filename, text.ToString());
                //Alternative: Serialize to JSON with custom jsonconveter saved to constant file
                File.WriteAllText(@"c:\001.txt", new JsonCreator().JsonFolder(fol, -1));
            }
            catch (Exception ex)
            {
                lb5.Content = "Enter or Select file correctly " + ex.Message;
            }
            lb5.Content = filename;
            lb6.Content = "Processed: " + GetAllFoldersCount(folname) + " folders";

        }

        public String CreateCustomJson(Folder folder) {
            string json2;
            using (var sw = new StringWriter())
            {
                using (var jtw = new JsonTextWriter(sw)
                {
                    Formatting = Formatting.Indented,
                    Indentation = 5,
                    IndentChar = ' '
                })
                {
                    (new JsonSerializer()).Serialize(jtw, folder);
                }
                json2 = sw.ToString();
            }
            return json2;
        }

        public int GetAllFoldersCount(String folder) {
            return Directory.GetDirectories(folder,"*",SearchOption.AllDirectories).Length;
        }
         */

    }   
}
